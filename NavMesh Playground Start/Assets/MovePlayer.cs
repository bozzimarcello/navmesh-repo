﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class MovePlayer : MonoBehaviour
{
    private NavMeshAgent agent;

    void Start()
    {
        agent = GetComponent<NavMeshAgent>();           
    }

    void Update()
    {
        if(Input.GetMouseButtonDown(0))
        {
            Vector3 mouseClickPosition = Input.mousePosition;
            Debug.Log("mouseClickPosition: " + mouseClickPosition);
            Ray ray = Camera.main.ScreenPointToRay(mouseClickPosition);
            Debug.DrawRay(ray.origin, ray.direction * 100, Color.red, 1f);

            RaycastHit hitInfo;
            bool hasHit = Physics.Raycast(ray, out hitInfo);
            if (hasHit)
            {
                agent.destination = hitInfo.point;
            }
        }
    }
}
